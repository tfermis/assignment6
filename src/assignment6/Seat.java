package assignment6;
 
/** The Seat class, represents seat in the theater with location and reservation information
 *  Contains getters and a setter for whether seat is reserved or not
 */

 public class Seat 
 {
 	private String row; // row identifier
 	private String houseLocation; // house that seat is located in
 	private int seatNumber; // seat number
 	private int rowNum; // number associated with row letter
 	private Client customer; // customer that seat is reserved for
 	private boolean reserved; // indicates whether seat is reserved or not

 	/**
 	 * Determines whether seat has been reserved or not
 	 * @return reserved - true if reserved, false if not reserved
 	 */
	public boolean isReserved() {return reserved;}
	/**
	 * 
	 * @param inputRow  The letter corresponding to the row where the seat is stored
	 * @param seatNum	  Seat number in the theater 
	 * @param rownum	Starting at row 0, the row in the matrix where the seat is stored
	 * @param house	The theater  (left, mid, right) where seat is stored
	 */
	public Seat(String inputRow, int seatNum, int rownum, String house)
 	{
 		row = inputRow;
 		seatNumber = seatNum;
 		rowNum = rownum;
 		reserved = false;
 		houseLocation = house;
 	}
 	
	/**
	 * Getter for seat's row letter
	 * @return row - seat row letter
	 */
	public String getRow(){	return row;}
	
	/**
	 * Getter for number associated with seat's row letter
	 * @return rowNum - number associated with seat's row letter
	 */
	public int getRowNum(){return rowNum;}

	/**
	 * Sets seat as reserved or not reserved
	 * @param reserved - true if seat is to be reserved, false if seat reservation is to be cancelled
	 * @param cust - customer whose seat is being reserved or unreserved
	 */
	public void setReserved(boolean reserved, Client cust) 
	{
		this.reserved = reserved;
		this.customer = cust;
	}

	/**
	 * Getter for House location of seat
	 * @return houseLocation - House location of seat
	 */
	public String getHouseLocation() {return houseLocation;}
	
	/**
	 * Getter for seat number
	 * @return seatNumber - seat number
	 */
	public int getSeatNumber(){return seatNumber;}

 }