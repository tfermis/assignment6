package assignment6;

/** The Client class, represents a client with an ID number associated with it
 * 
 */
public class Client 
{
	private int ID; // number associated with client
	public Client(int number)
	{
		ID = number;
	}
}
