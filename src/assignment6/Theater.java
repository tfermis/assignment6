package assignment6;

import java.lang.reflect.Array;

/** The Theater class, represents seating configuration for Bates Recital Hall
 *  Contains methods for finding and reserving seats and printing seat reservations
 *  
 */
public class Theater 
{
	/*
	 * Seating configuration for house left
	 */
	private int numRowsTheaterLeft = 22; //number of rows in theater house left
	private int numSeatsPerRowLeft = 7;	 //number of seats per row in house left
	String [] rowLettersTheaterLeft = {"C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z","AA"}; //array representing row configuration in house left
	//Ex: row 0 in the matrix is equivalent to row "C" in the array above
	Seat myTheaterLeft[][] = new Seat [numRowsTheaterLeft][numSeatsPerRowLeft];	//matrix representing seating configuration in house left, where row number corresponds to index in rowLettersTheaterLeft
	private int bestRowLeft = 0; // tracker for lowest row where seats are available in house left
	private int scalingTheaterLeft = 2; // scaling used to compare theater left rows to other theater rows (because theater left rows begin at row (2) compared to the other theaters, must adjust to compare)
	boolean leftNotFull = true; // indicator for whether or not house left is full
	
	/*
	 * Seating configuration for house mid
	 */
	private int numRowsTheaterMid = 22; //number of rows in theater house mid
	private int numSeatsPerRowMid = 14; //number of seats per row in house mid
	String [] rowLettersTheaterMid  = {"A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X"}; //matrix representing row configuration in house mid
	//Ex: row 0 in the matrix is equivalent to row "A" in the array above
	Seat myTheaterMid[][] = new Seat [numRowsTheaterMid][numSeatsPerRowMid]; //matrix representing seating configuration in house mid, where row number corresponds to index in rowLettersTheaterMid
	private int bestRowMid = 0; // tracker for lowest row where seats are available in house mid
	boolean midNotFull = true; // indicator for whether or not house mid is full
	
	/*
	 * Seating configuration for house right
	 */
	private int numRowsTheaterRight = 24; //number of rows in theater house right
	private int numSeatsPerRowRight = 7;  //number of seats per row in house right
	String [] rowLettersTheaterRight = {"A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z","AA"}; //matrix representing row configuration in house right
	//Ex: row 2 in the matrix is equivalent to row "C" in the array above
	Seat myTheaterRight[][] = new Seat [numRowsTheaterRight][numSeatsPerRowRight]; //matrix representing seating configuration in house right, where row number corresponds to index in rowLettersTheaterRight
	private int bestRowRight = 0; // tracker for lowest row where seats are available in house right
	boolean rightNotFull = true;  // indicator for whether or not house right is full
	
    /*
     * Seating configuration in row AA
     */
	Seat [] specialSeats = new Seat [9]; // array for seats in row AA
	String [] specialSeatNums = {"L101","L102","L103","L104", "L116","L117","L118", "R127","R128"}; // enumerated list of seat labels for entire row AA
	private int numSpecialSeats = 9; // number of seats in row AA
	
	private int startSeatLeft = 101; //Starting seat number in house left
	private int startSeatMid = 108;	 //Starting seat number in house mid
	private int startSeatRight = 122;//Starting seat number in house right
	
	private int rowAdjustmentScaling = 1; //scaling to store row number in matrix. Ex: row number may be 1, but must be stored as the 0th row in matrix
	
	/**
	 * Initializes all three theater matrices with seats for each element in matrix (each seat object will contain location). Also initializes the special array
	 * (for the row not included in the matrices, AA)
	 */
	public Theater() 
	{
		for(int i = 0; i<numRowsTheaterLeft; i++)		//initializes seats in matrix object
		{
			for(int j=0; j<numSeatsPerRowLeft;j++)
			{
				myTheaterLeft[i][j] = new Seat(rowLettersTheaterLeft[i],j+startSeatLeft,i,"L");			//so this will send (in order) the row letter, the row num, the seat, and the theater (L == house left , etc) 
			}
		}
		for(int i =0; i< numRowsTheaterMid; i++)
		{
			for(int j=0; j<numSeatsPerRowMid;j++)
			{
				myTheaterMid[i][j] = new Seat(rowLettersTheaterMid[i],j+startSeatMid,i,"M");
			}
		}
		for(int i =0; i< numRowsTheaterRight; i++)
		{
			for(int j=0; j<numSeatsPerRowRight; j++)
			{
				myTheaterRight[i][j] = new Seat(rowLettersTheaterRight[i],j+startSeatRight,i,"R");
			}
		}
		
		//initialization of special seat row (special seats have to be manually entered in because of THE ABSOLUTELY STRANGE AA ROW. SERIOUSLY, IT BROKE THE MATRIX)
		for(int i =0; i< numSpecialSeats; i++)
		{
			String seatInfo = specialSeatNums[i];
			specialSeats[i] = new Seat("AA", Integer.parseInt(seatInfo.substring(1)), 25, seatInfo.charAt(0) + ""); //
		}
	}
	/**
	 * Where the magic happens. So the way this works is we have a best row number for all three theater houses. By comparing the best row number for all three theater houses,
	 * we can give priority to seats in the middle front, then the left and right front, then middle back, then left and right back. If this best row number matches the number
	 * of rows in the respective theater house, the theater house is filled and we can ignore and move on to fill the other houses. To fill in seats in a theater,
	 * we select the next theater house, based on the above best row number, and linearly go through the seats in the row. If the current seat is not reserved,
	 * we select the seat. Once a seat has been selected, we assign the seat as "marked" and print the ticket for the customer. Continue using this algorithm until we hit the special row,
	 * in which we promptly BREAK DOWN AND CRY BECAUSE IT DESTROYED OUR MATRIX ASSIGNMENTS, AHHHHHHHH, and then fill in linearly.
	 * @param cust = client
	 * @return best seat based on above algorithm
	 */
	public synchronized Seat bestAvailableSeat(Client cust)			
	{
		//so check first seat, if reserved, check last, if reserved, go to next row
		boolean fillSeat = false;
		if(bestRowMid >= numRowsTheaterMid)		//check if house is filled
		{
			midNotFull = false;
		}
		if(bestRowRight >= numRowsTheaterRight)
		{
			rightNotFull = false;
		}
		if(bestRowLeft >= numRowsTheaterLeft)
		{
			leftNotFull = false;
		}
		
		if(bestRowMid <= bestRowLeft+scalingTheaterLeft && bestRowMid <= bestRowRight && midNotFull)		//middle is open, fill middle first
		{
			for(int i = 0; i < numSeatsPerRowMid; i++)		//go until seat is filled
			{
				if(!(myTheaterMid[bestRowMid][i].isReserved()))		//linearly go through row looking for seats not filled
				{
					if(i == numSeatsPerRowMid - rowAdjustmentScaling)		//last seat, update the best row
					{
						bestRowMid ++;
						markAvailableSeatTaken(myTheaterMid[bestRowMid-rowAdjustmentScaling][i],cust); //mark seat before returning
						printTicketSeat(myTheaterMid[bestRowMid-rowAdjustmentScaling][i]);// print reservation
						return myTheaterMid[bestRowMid-rowAdjustmentScaling][i];
					}
					markAvailableSeatTaken(myTheaterMid[bestRowMid][i],cust); //mark seat taken before returning
					printTicketSeat(myTheaterMid[bestRowMid][i]);// print reservation
					return myTheaterMid[bestRowMid][i];
				}
			}
		}
		else if (bestRowRight <= (bestRowLeft+scalingTheaterLeft) && rightNotFull)
		{
			for(int i = 0; i < numSeatsPerRowRight; i++)		//go until seat is filled
			{
				if(!(myTheaterRight[bestRowRight][i].isReserved()))
				{
					if(i == numSeatsPerRowRight - rowAdjustmentScaling)		//last seat, update the best row
					{
						bestRowRight++;
						markAvailableSeatTaken(myTheaterRight[bestRowRight-rowAdjustmentScaling][i],cust);//mark seat before returning, rows are subtracted by -1 because row numbers do not start @ 0 (realism)
						printTicketSeat(myTheaterRight[bestRowRight-rowAdjustmentScaling][i]);// print reservation
						return myTheaterRight[bestRowRight-rowAdjustmentScaling][i];
					}
					markAvailableSeatTaken(myTheaterRight[bestRowRight][i],cust);//mark seat before returning
					printTicketSeat(myTheaterRight[bestRowRight][i]);// print reservation
					return myTheaterRight[bestRowRight][i];
				}
			}
		}
		else if (leftNotFull)
		{
			for(int i = 0; i < numSeatsPerRowLeft; i++)		//go until seat is filled
			{
				if(!(myTheaterLeft[bestRowLeft][i].isReserved()))
				{
					if(bestRowLeft==0)		//special case for the first row in theater left, THERE IS NO SEAT 107... WHHHYYYYYY
					{
						if(i == numSeatsPerRowLeft - 2)	//skips seat number 107 in the row
						{
							bestRowLeft ++;
							markAvailableSeatTaken(myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i],cust);//mark seat before returning
							markAvailableSeatTaken(myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i+1],cust);//mark seat before returning
							printTicketSeat(myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i]);// print reservation
							return myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i];
						}
					}
					if(i == numSeatsPerRowLeft - rowAdjustmentScaling)		//last seat, update the best row
					{
						bestRowLeft ++;
						markAvailableSeatTaken(myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i],cust);//mark seat before returning
						printTicketSeat(myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i]);// print reservation
						return myTheaterLeft[bestRowLeft-rowAdjustmentScaling][i];
					}
					markAvailableSeatTaken(myTheaterLeft[bestRowLeft][i],cust);//mark seat before returning
					printTicketSeat(myTheaterLeft[bestRowLeft][i]);// print reservation
					return myTheaterLeft[bestRowLeft][i];
				}
			}
		}
		for(int i=0; i< numSpecialSeats; i++) //special case, just go through this array and fill linearly
		{
			if(!(specialSeats[i].isReserved()))
			{
				markAvailableSeatTaken(specialSeats[i],cust);//mark seat before returning
				printTicketSeat(specialSeats[i]); // print reservation
				return specialSeats[i];
			}
		}
		return null;		//no available seat
	}
	
	/**
	 * Reserves a particular seat in the theater by marking it as reserved (using setReserved method in Seat)
	 * The method identifies the house in which the seat is located using the Seat getHouseLocation() method. Once the
	 * house location is determined, the method accesses the theater's matrix that is associated with that house at the
	 * index corresponding to the correct seat number. It then reserves the Seat object located at that index.
	 * @param s - seat being reserved
	 * @param cust - targeted customer for seat reservation
	 */
	public void markAvailableSeatTaken(Seat s, Client cust)	
	{
		if(s.getHouseLocation().equals("L") && s.getRowNum()<25) // Seat is located in house left, excluding row AA
		{
			myTheaterLeft[s.getRowNum()][s.getSeatNumber()-startSeatLeft].setReserved(true,cust); // Reserve seat, which is located in house left seat array
		}
		else if (s.getHouseLocation().equals("M") && s.getRowNum()<25) // Seat is located in house mid, excluding row AA
		{
			myTheaterMid[s.getRowNum()][s.getSeatNumber()-startSeatMid].setReserved(true,cust); // Reserve seat, which is located in house mid seat array
		}
		else if (s.getHouseLocation().equals("R") && s.getRowNum()<25) // Seat is located in house right, excluding row AA
		{
			myTheaterRight[s.getRowNum()][s.getSeatNumber()-startSeatRight].setReserved(true,cust); // Reserve seat, which is located in house right seat array
		}
		else // Seat is located in row AA
		{
			for(int i =0; i<numSpecialSeats; i++) //traverse row AA seats array until seat number match is found
			{
				if(specialSeats[i].getSeatNumber() == s.getSeatNumber())
				{
					specialSeats[i].setReserved(true,cust); // Reserve seat, which is located in row AA seats array
				}
			}
		}
	}
	
	/**
	 * Prints seat reservation information (Office that reserved seat, House location, seat number, seat row)
	 * @param s - reserved seat
	 */
	public void printTicketSeat(Seat s)
	{
		Thread current = Thread.currentThread(); //Obtain the particular box office (current thread's name) that made the reservation
		System.out.println(current.getName() + ": Reserved House " + s.getHouseLocation() + ", " + s.getSeatNumber() + s.getRow());
	}
	
}
