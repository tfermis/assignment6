package assignment6;
import java.util.*;

/** The OfficeThread class, represents box office that services customer lines. Runs on a thread
 *  Creates customer lines and services each customer by invoking method that finds and reserves seats
 * 
 */

public class OfficeThread implements Runnable
{
	private Theater theater; // seat configuration of theater
	final private int DELAY = 1; // 1 ms delay between ticket reservations for interleaving
	
	/**
	 * 
	 * @param th Theater office thread is set to assign seats to
	 */
	public OfficeThread(Theater th)
	{
		this.theater = th;
	}
	
	/** 
	 * Creates customer queues and services each customer
	 * Replenishes customer queues when empty
	 */
	public void run()
	{
		try
		{
			Random newCustomers = new Random();
			int firstcustomers = (newCustomers.nextInt(10)+1)*100;
			ArrayList<Client> custline = new ArrayList<Client>(firstcustomers); //simulating initial customer queue (size range 100-1000)
			for(int k=0; k<firstcustomers; k++)
			{
				custline.add(new Client(k)); // populate with customers
			}
			for(int i=0; i<custline.size(); i++)
			{
				serviceCust(custline.get(i)); //service the customer
				
				if(i == custline.size()-1) //customer line empty, new customers come in (random size in range 100-1000)
				{
					int incomingID = custline.size(); // offset for incoming customer IDs (keeps incrementing)
					int incomingCustomers = ((newCustomers.nextInt(10)+1)*100);
					for(int j=0; j<incomingCustomers; j++)
					{
						custline.add(new Client(incomingID + j)); // populate with customers
					}
				}
			}
		}
		catch(NoSeatException e) // thrown when no seats are available
		{
			System.out.println("Sorry, we are sold out");
		}
	}
	
	/**
	 * Services customer by invoking method for finding and reserving seat in the theater
	 * @param cust - customer being serviced
	 */
	public void serviceCust (Client cust)
	{
		Seat bestSeat = theater.bestAvailableSeat(cust); //find and reserve seat for customer. Print out reservation.
		if(bestSeat != null)
		{
			try{
				Thread.sleep(DELAY); // 1ms delay for thread interleaving
			}
			catch(InterruptedException IE){}
		}
		else
		{
			throw new NoSeatException(); // no seats left
		}	
	}
}