package assignment6;
/**
 * Thrown whenever there are no more seats to sell. Thrown by office thread
 */
public class NoSeatException extends RuntimeException
{
	/**
	 * Creates message of "NoSeatException" to send to super constructor
	 */
	public NoSeatException() // default constructor
	{
		super("NoSeatException");
	}
	
	/**
	 * Creates message of the input string message to send to super constructor
	 * @param message takes in a message as a string, displays message by call to the super class
	 */
	public NoSeatException(String message)
	{// constructor takes a String as a parameter
		super(message);
	}
}



