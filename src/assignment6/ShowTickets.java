package assignment6;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.Random;

/** The ShowTickets class, driver class for the box office threads
 *  Initializes theater seat configuration and runs concurrent threads that service customers who are purchasing tickets
 * 
 *  @author Davin Siu
 *  @author Thomas Ermis
 *  EE 422C, Spring 2015
 *  TA: Jo Egner
 *  Section: Thursday, 3:30PM
 */
public class ShowTickets 
{
	public static void main(String[] args) 
	{
		Theater bates = new Theater(); // Initialize seat configuration
		/*
		 * Initialize Box office threads
		 */
		OfficeThread a = new OfficeThread(bates);	//Initializes threads with common theater to work on (layout of bates theater)
		Thread officeA = new Thread(a);
		officeA.setName("Office A");					//set to identify which box office assigns tickets
		OfficeThread b = new OfficeThread(bates);
		Thread officeB = new Thread(b);
		officeB.setName("Office B");
		OfficeThread c = new OfficeThread(bates);
		Thread officeC = new Thread(c);
		officeC.setName("Office C");
		
		/*
		 * Run Box office threads
		 */
		officeA.start();
		officeB.start();
		officeC.start();
	}
}
