package assignment6;

public class Seat {
	private String row; // row identifier
	private Integer seatNumber; // seat number
	
	public void Seat(String inputRow, int inputNumber)
	{
		row = inputRow;
		seatNumber = inputNumber;
	}
	
	public String getRow()
	{
		return row;
	}
	
	public String getSeat()
	{
		return seatNumber.toString();
	}
}
