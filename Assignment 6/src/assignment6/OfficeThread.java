package assignment6;
import java.util.*;

public class OfficeThread implements Runnable{
private Theater theater;
final private int DELAY = 1000; // 1 second delay between ticket reservations

public OfficeThread(Theater th)
{
	this.theater = th;
}

public void run()
{
	Random newCustomers = new Random();
	int firstcustomers = (newCustomers.nextInt(10)+1)*100;
	ArrayList<Integer> custline = new ArrayList<Integer>(firstcustomers); //simulating initial customer queue (range 100-1000)
	for(int k=0; k<firstcustomers; k++)
	{
		custline.add(1);
	}
	for(int i=0; i<custline.size(); i++)
	{
		Seat bestSeat = theater.bestAvailableSeat();
		if(bestSeat != null)
		{
			theater.markAvailableSeatTaken(bestSeat);
			theater.printTicketSeat(bestSeat);
			try{
				Thread.sleep(DELAY);
			}
			catch(InterruptedException IE){}
		}
			else
			{
				throw new NoSeatException();
			}
			
		if(i == custline.size()-1) //customer line empty, new customers come in (random size in range 100-1000)
		{
			int incomingCustomers = ((newCustomers.nextInt(10)+1)*100);
			for(int j=0; j<incomingCustomers; j++)
			{
				custline.add(1);
			}
		}
	}
}




}